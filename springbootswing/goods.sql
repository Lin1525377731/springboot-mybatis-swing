/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50730
Source Host           : localhost:3306
Source Database       : goods

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-07-29 12:11:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(20,2) DEFAULT NULL,
  `number` double(20,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('3', '冰红茶', '3.00', '40');
INSERT INTO `goods` VALUES ('1', '娃哈哈', '9.00', '50');
INSERT INTO `goods` VALUES ('2', '苏打水', '4.00', '55');
INSERT INTO `goods` VALUES ('15', '王老吉', '77.77', '1');
INSERT INTO `goods` VALUES ('16', 'hhh', '23.00', '3');
INSERT INTO `goods` VALUES ('13', '测试类', '101.00', '18');
