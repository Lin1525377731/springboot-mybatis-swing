package com.sise.springbootswing;

import com.sise.springbootswing.JFrame.GoodsJFrame;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

@MapperScan("com.sise.springbootswing.mapper")
@SpringBootApplication(scanBasePackages = {"com.sise.springbootswing"})
public class SpringbootswingApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringbootswingApplication.class);
        ApplicationContext context = builder.headless(false).run(args);
//        ApplicationContext context = builder.headless(true).run(args);
        GoodsJFrame hjf = context.getBean(GoodsJFrame.class);

        hjf.setVisible(true);
//        SpringApplication.run(SpringbootswingApplication.class, args);
    }

}
