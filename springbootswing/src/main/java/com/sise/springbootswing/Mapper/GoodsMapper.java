package com.sise.springbootswing.Mapper;

import com.sise.springbootswing.DAO.GoodsDAO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface GoodsMapper {
//    商品列表
    List<GoodsDAO> queryAllList();
//    商品信息
    List<GoodsDAO> queryGoodsMessage(String name);
//    添加商品
    int addGoodmessage(String name,double price,double number);
//    更新商品数据
    int updateGoodsMessage(String name,double price,double number);
//    删除商品
    int deleteGoods(String name);
}
