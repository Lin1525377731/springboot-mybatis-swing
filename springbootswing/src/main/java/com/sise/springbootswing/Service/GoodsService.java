package com.sise.springbootswing.Service;

import com.sise.springbootswing.DAO.GoodsDAO;
import com.sise.springbootswing.Mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GoodsService implements GoodsMapper {

    @Autowired
    GoodsMapper goodsMapper;

    /**
     * 返回商品列表
     * @return
     */
    @Override
    public List<GoodsDAO> queryAllList() {
        System.out.println("查询成功");
        return goodsMapper.queryAllList();
    }

    /**
     * 通过商品名称查询
     * @param name
     * @return
     */
    @Override
    public List<GoodsDAO> queryGoodsMessage(String name) {
        System.out.println("查询商品信息成功");
        return goodsMapper.queryGoodsMessage(name);
    }

    /**
     * 添加商品
     * @param name
     * @param price
     * @param number
     * @return
     */
    @Override
    public int addGoodmessage(String name,double price,double number) {
        int result = goodsMapper.addGoodmessage(name,price,number);
        if (result==1) {
            System.out.println("添加成功");
            return 1;
        }else {
            System.out.println("添加失败");
            return 0;
        }
    }

    @Override
    public int updateGoodsMessage(String name, double price, double number) {
        int result = goodsMapper.updateGoodsMessage(name,price,number);
        if (result==1) {
            System.out.println("更新成功");
            return 1;
        }else {
            System.out.println("更新失败");
            return 0;
        }
    }

    @Override
    public int deleteGoods(String name) {
        int result = goodsMapper.deleteGoods(name);
        if (result==1) {
            System.out.println("删除成功");
            return 1;
        }else {
            System.out.println("删除失败");
            return 0;
        }
    }
}
