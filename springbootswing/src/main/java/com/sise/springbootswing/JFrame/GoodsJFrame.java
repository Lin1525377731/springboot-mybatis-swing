package com.sise.springbootswing.JFrame;

import com.sise.springbootswing.DAO.GoodsDAO;
import com.sise.springbootswing.Service.GoodsService;
import com.sise.springbootswing.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
//@Component
@Service("goodsJFrame")
public class GoodsJFrame extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTable table;

//    public static final String[] TABLE_HEAD = { "商品编号", "商品名称", "价格", "库存数量" };
//    public Object[][] tableBody = new Object[15][4];

    @Autowired
    private GoodsService goodsService;

    /**
     * Create the frame.
     */
    public GoodsJFrame() {
        setTitle("商品管理系统");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1050, 702);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
//        DefaultTableModel tableModel = new DefaultTableModel(tableBody, TABLE_HEAD);
//        java.util.List<GoodsDAO> list = goodsService.queryAllList();
//				显示框
//				List<NodesDAO> list = nodesService.findByProjectNameAndDeviceNo(projectName.getText(),deviceNo.getText());
//        tableBody = new Object[list.size()][4];
//        for (int i = 0; i < list.size(); i++) {
//            tableBody[i][0]=list.get(i).getId();
//            tableBody[i][1]=list.get(i).getName();
//            tableBody[i][2]=list.get(i).getPrice();
//            tableBody[i][3]=list.get(i).getNumber();
//        }
//        tableModel.setDataVector(tableBody, TABLE_HEAD);
        JButton deviceDetails = new JButton("商品增加");
        deviceDetails.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsDetailsJFrame frame = SpringUtil.getBean(GoodsDetailsJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        deviceDetails.setBounds(14, 29, 181, 49);
        contentPane.add(deviceDetails);

        JButton deviceStatus = new JButton("商品删除");
        deviceStatus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsStatusJFrame frame = SpringUtil.getBean(GoodsStatusJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        deviceStatus.setBounds(300, 29, 181, 49);
        contentPane.add(deviceStatus);

        JButton deviceData = new JButton("商品修改");
        deviceData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsDataJFrame frame = SpringUtil.getBean(GoodsDataJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        deviceData.setBounds(577, 29, 181, 49);
        contentPane.add(deviceData);

        JButton btnNewButton = new JButton("商品查询");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsQueryJFrame frame = SpringUtil.getBean(GoodsQueryJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        btnNewButton.setBounds(837, 29, 181, 49);
        contentPane.add(btnNewButton);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(14, 103, 1004, 539);
        contentPane.add(scrollPane);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                this.setToolSystemTray();
            }

            private void setToolSystemTray() {
//				JFrame jf = new JFrame("系统托盘测试");
                // 系统是否支持系统托盘
                if (SystemTray.isSupported()) {

                    // 获取SystemTray系统托盘实例
                    SystemTray tray = SystemTray.getSystemTray();
                    System.out.println(getClass().getClassLoader()
                            .getResource(""));
                    // 加载图片
                    ImageIcon image = new ImageIcon("src/main/resources/static/1.png");
                    // 弹出式 菜单
                    PopupMenu popup = new PopupMenu();

                    // 创建关于菜单项
                    MenuItem aboutItem = new MenuItem("open");
                    aboutItem.addActionListener(new ActionListener(){

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            setVisible(true);

                        }

                    });
                    popup.add(aboutItem);

                    // 创建退出菜单项
                    MenuItem exitItem = new MenuItem("exit");
                    exitItem.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            System.exit(0);
                        }
                    });
                    popup.add(exitItem);
                    // 构造托盘图标
                    TrayIcon trayIcon = new TrayIcon(image.getImage(), "乖乖", popup);
                    trayIcon.setImageAutoSize(true);
                    // 鼠标事件
                    trayIcon.addMouseListener(new MouseAdapter() {

                        public void mouseClicked(MouseEvent e) {
                            // 单击显示窗口
                            if (e.getClickCount() == 1) {
                                setVisible(true);
                            }
                        }
                    });

                    // 添加托盘图标
                    try {
                        tray.add(trayIcon);
                    } catch (AWTException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            System.getProperty("os.version") + "系统不支持系统托盘!");
                }
            }
        });

        table = new JTable();
        table.setFont(new Font("微软雅黑", Font.PLAIN, 18));
        table.setRowHeight(30);
//        table.setModel(tableModel);
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class, r);
        scrollPane.setColumnHeaderView(table);
        scrollPane.setViewportView(table);

    }

    /* 实现系统托盘化 */
    protected void setToolSystemTray() {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }
}
