package com.sise.springbootswing.JFrame;

import com.sise.springbootswing.Service.GoodsService;
import com.sise.springbootswing.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class GoodsDetailsJFrame extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField id;
    private JTextField name;
    private JTextField price;
    private JTextField number;

    String sql;
    String nname;
    double pprice;
    int iid,nnumber;

    @Autowired
    private GoodsService goodsService;
    //public static final String[] TABLE_HEAD = { "项目ID", "设备ID", "项目名称", "设备编号" };
    //public Object[][] tableBody = new Object[20][4];
    //private JTable table;

    /**
     * Create the frame.
     */
    public GoodsDetailsJFrame() {
        setTitle("商品增加");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1050, 650);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
//		DefaultTableModel tableModel = new DefaultTableModel(tableBody, TABLE_HEAD);

        JButton btnNewButton = new JButton("返回首页");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsJFrame frame = SpringUtil.getBean(GoodsJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        btnNewButton.setBounds(835, 541, 113, 27);
        contentPane.add(btnNewButton);



        JLabel lid = new JLabel("商品编号：");
        lid.setBounds(429, 30, 98, 18);
        contentPane.add(lid);

        JLabel lname = new JLabel("商品名称：");
        lname.setBounds(44, 30, 98, 18);
        contentPane.add(lname);

        JLabel lprice = new JLabel("商品价格：");
        lprice.setBounds(429, 60, 98, 18);
        contentPane.add(lprice);

        JLabel lnumber = new JLabel("商品数量：");
        lnumber.setBounds(44, 60, 98, 18);
        contentPane.add(lnumber);

        id = new JTextField();
        id.setText("");
        id.setBounds(509, 30, 274, 24);
        contentPane.add(id);
        id.setColumns(10);

        name = new JTextField();
        name.setText("");
        name.setBounds(128, 30, 274, 24);
        contentPane.add(name);
        name.setColumns(10);

        price = new JTextField();
        price.setText("");
        price.setBounds(509, 60, 274, 24);
        contentPane.add(price);
        price.setColumns(10);

        number = new JTextField();
        number.setText("");
        number.setBounds(128, 60, 274, 24);
        contentPane.add(number);
        number.setColumns(10);

        JButton button = new JButton("增加");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                DBHelper.getConnection();
//                DBHelper.openStatement();
//
                iid = Integer.parseInt(id.getText());
                nnumber = Integer.parseInt(number.getText());
                pprice = Double.parseDouble(price.getText());
                nname =name.getText();
//                sql = iid + ",'" + nname + "'," + pprice + "," + nnumber ;
//                String sql2 = "values("+ sql +")";
//                DBHelper.update("insert into goods(id, name, price, number)" + sql2);
//                DBHelper.close();
                goodsService.addGoodmessage(nname,nnumber,pprice);
                JOptionPane.showMessageDialog(null, "添加成功");
//				List<Device> deviceList = deviceService.findByProjectNameAndDeviceNo(projectName.getText(),
//						deviceNo.getText());
//				tableBody = new Object[deviceList.size()][4];
//				for (int i = 0; i < deviceList.size(); i++) {
//					tableBody[i][0] = deviceList.get(i).getConstruction_id();
//					tableBody[i][1] = deviceList.get(i).getId();
//					tableBody[i][2] = deviceList.get(i).getProject_name();
//					tableBody[i][3] = deviceList.get(i).getDvice_no();
//				}
//				tableModel.setDataVector(tableBody, TABLE_HEAD);
            }
        });
        button.setBounds(835, 29, 113, 60);
        contentPane.add(button);



//		JScrollPane scrollPane = new JScrollPane();
//		scrollPane.setBounds(41, 91, 937, 413);
//		contentPane.add(scrollPane);

//		table = new JTable();
//		table.setFont(new Font("微软雅黑", Font.PLAIN, 20));
//		table.setRowHeight(30);                    //设置行高度
//		table.setModel(tableModel);                //设置内容
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
//		table.setDefaultRenderer(Object.class,r);
//		scrollPane.setColumnHeaderView(table);
//		scrollPane.setViewportView(table);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
    }
}
