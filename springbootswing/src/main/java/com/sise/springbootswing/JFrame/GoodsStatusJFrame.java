package com.sise.springbootswing.JFrame;

import com.sise.springbootswing.Service.GoodsService;
import com.sise.springbootswing.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
@Component
public class GoodsStatusJFrame extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField projectName;
    private JTextField deviceNo;

    public static final String[] TABLE_HEAD = { "商品编号", "商品名称", "价格", "库存数量" };
    public Object[][] tableBody = new Object[20][4];
    private JTable table;
    @Autowired
    private GoodsService goodsService;
    /**
     * Create the frame.
     */
    public GoodsStatusJFrame() {
        setTitle("商品删除");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1050, 650);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        DefaultTableModel tableModel = new DefaultTableModel(tableBody, TABLE_HEAD);

        JButton btnNewButton = new JButton("返回首页");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsJFrame frame = SpringUtil.getBean(GoodsJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        btnNewButton.setBounds(872, 540, 113, 27);
        contentPane.add(btnNewButton);

//		JButton btnNewButton_1 = new JButton("设备详情查询");
//		btnNewButton_1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				DeviceDetailsJFrame frame = SpringUtil.getBean(DeviceDetailsJFrame.class);
//				frame.setVisible(true);
//				dispose();
//			}
//		});
//		btnNewButton_1.setBounds(688, 540, 138, 27);
//		contentPane.add(btnNewButton_1);
//
//		JButton btnNewButton_2 = new JButton("设备数据查询");
//		btnNewButton_2.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				DeviceDataJFrame frame = SpringUtil.getBean(DeviceDataJFrame.class);
//				frame.setVisible(true);
//				dispose();
//			}
//		});
//		btnNewButton_2.setBounds(500, 540, 143, 27);
//		contentPane.add(btnNewButton_2);

        JLabel label = new JLabel("商品名称：");
        label.setBounds(40, 38, 85, 18);
        contentPane.add(label);

//		JLabel label_1 = new JLabel("减少数量：");
//		label_1.setBounds(407, 35, 85, 18);
//		contentPane.add(label_1);

        projectName = new JTextField();
        projectName.setBounds(125, 35, 234, 24);
        contentPane.add(projectName);
        projectName.setColumns(10);

//		deviceNo = new JTextField();
//		deviceNo.setText("");
//		deviceNo.setColumns(10);
//		deviceNo.setBounds(487, 32, 234, 24);
//		contentPane.add(deviceNo);

        JButton btnNewButton_3 = new JButton("删除");
        btnNewButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                  String nname = projectName.getText();
//                DBHelper.getConnection();
//                DBHelper.openStatement();
//                String sql = "delete from goods where name='" + projectName.getText() +"'";
//                System.out.println(sql);
//                DBHelper.update(sql);
//                DBHelper.close();
                goodsService.deleteGoods(nname);
                JOptionPane.showMessageDialog(null, "删除成功");
//				List<DeviceStatus> list = constructionDeviceService.findDeviceStatus(projectName.getText(),
//						deviceNo.getText());
//				tableBody = new Object[list.size()][7];
//				for (int i = 0; i < list.size(); i++) {
//					tableBody[i][0] = list.get(i).getConstruction_id();
//					tableBody[i][1] = list.get(i).getId();
//					tableBody[i][2] = list.get(i).getProject_name();
//					tableBody[i][3] = list.get(i).getDvice_no();
//					tableBody[i][4] = list.get(i).getPid() == 0 ? "离线" : "在线";
//					tableBody[i][5] = list.get(i).getName();
//					tableBody[i][6] = list.get(i).getMobile();
//				}
//				tableModel.setDataVector(tableBody, TABLE_HEAD);
            }
        });
        btnNewButton_3.setBounds(480, 30, 113, 27);
        contentPane.add(btnNewButton_3);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(40, 87, 950, 432);
        contentPane.add(scrollPane);

        table = new JTable();
        table.setFont(new Font("微软雅黑", Font.PLAIN, 18));
        table.setRowHeight(30);
        table.setModel(tableModel);
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class,r);
        scrollPane.setColumnHeaderView(table);
        scrollPane.setViewportView(table);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }
}
