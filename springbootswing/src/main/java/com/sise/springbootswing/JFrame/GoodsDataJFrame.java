package com.sise.springbootswing.JFrame;

import com.sise.springbootswing.DAO.GoodsDAO;
import com.sise.springbootswing.Service.GoodsService;
import com.sise.springbootswing.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
@Component
@Service("goodsDataJFrame")
public class GoodsDataJFrame extends JFrame implements ActionListener {

    private JTextField id;
    private JTextField name;
    private JTextField price;
    private JTextField number;

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField projectName;
    private JTextField deviceNo;
    private JTable table;

    public static final String[] TABLE_HEAD = { "商品编号", "商品名称", "价格", "库存数量" };
    public Object[][] tableBody = new Object[15][4];
    String sql;
    String nname;
    double pprice;
    int iid,nnumber;

    @Autowired
    private GoodsService goodsService;

    /**
     * Create the frame.
     */
    public GoodsDataJFrame() {
        setTitle("商品修改");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1050, 650);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        DefaultTableModel tableModel = new DefaultTableModel(tableBody, TABLE_HEAD);

        JButton backHead = new JButton("返回首页");

        backHead.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
//                GoodsJFrame frame = SpringUtil.getBean(GoodsJFrame.class);
                GoodsJFrame frame = new GoodsJFrame();
                frame.setVisible(true);
                dispose();
            }
        });
        backHead.setBounds(694, 543, 113, 27);
        contentPane.add(backHead);



//		JLabel lid = new JLabel("商品编号：");
//		lid.setBounds(429, 30, 98, 18);
//		contentPane.add(lid);

        JLabel lname = new JLabel("商品名称：");
        lname.setBounds(44, 30, 98, 18);
        contentPane.add(lname);

        JLabel lprice = new JLabel("商品价格：");
        lprice.setBounds(429, 60, 98, 18);
        contentPane.add(lprice);

        JLabel lnumber = new JLabel("商品数量：");
        lnumber.setBounds(44, 60, 98, 18);
        contentPane.add(lnumber);

        name = new JTextField();
        name.setText("");
        name.setBounds(128, 30, 274, 24);
        contentPane.add(name);
        name.setColumns(10);

//		name = new JTextField();
//		name.setText("");
//		name.setBounds(509, 30, 274, 24);
//		contentPane.add(name);
//		name.setColumns(10);

        price = new JTextField();
        price.setText("");
        price.setBounds(509, 60, 274, 24);
        contentPane.add(price);
        price.setColumns(10);

        number = new JTextField();
        number.setText("");
        number.setBounds(128, 60, 274, 24);
        contentPane.add(number);
        number.setColumns(10);

        JButton btnNewButton = new JButton("修改");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
//                DBHelper.getConnection();
//                DBHelper.openStatement();
//
//                nnumber = Integer.parseInt(number.getText());
//                pprice = Double.parseDouble(price.getText());
//                nname =name.getText();
//                String sql2 = "number="+ nnumber +",price=" + pprice;
//                sql = "update goods set " + sql2 +" where name='" + nname + "'";
//                System.out.println(sql);
//                DBHelper.update(sql);
//                DBHelper.close();
                nnumber = Integer.parseInt(number.getText());
                pprice = Double.parseDouble(price.getText());
                nname =name.getText();
                goodsService.updateGoodsMessage(nname,pprice,nnumber);
                java.util.List<GoodsDAO> list = goodsService.queryAllList();
//				显示框
//				List<NodesDAO> list = nodesService.findByProjectNameAndDeviceNo(projectName.getText(),deviceNo.getText());
				tableBody = new Object[list.size()][4];
				for (int i = 0; i < list.size(); i++) {
			    	tableBody[i][0]=list.get(i).getId();
			    	tableBody[i][1]=list.get(i).getName();
			    	tableBody[i][2]=list.get(i).getPrice();
			    	tableBody[i][3]=list.get(i).getNumber();
				}
                tableModel.setDataVector(tableBody, TABLE_HEAD);
                JOptionPane.showMessageDialog(null, "修改成功");
            }
        });
        btnNewButton.setBounds(880, 20, 113, 60);
        contentPane.add(btnNewButton);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(38, 86, 950, 426);
        contentPane.add(scrollPane);
        table = new JTable();
        table.setFont(new Font("微软雅黑", Font.PLAIN, 18));
        table.setRowHeight(30);                    //设置行高度
        table.setModel(tableModel);
//		table.getColumnModel().getColumn(7).setPreferredWidth(190);
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class,r);
        scrollPane.setColumnHeaderView(table);
        scrollPane.setViewportView(table);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }
}
