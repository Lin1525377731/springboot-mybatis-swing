package com.sise.springbootswing.JFrame;

import com.sise.springbootswing.DAO.GoodsDAO;
import com.sise.springbootswing.Service.GoodsService;
import com.sise.springbootswing.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Component
public class GoodsQueryJFrame extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField projectName;
    private JTextField deviceNo;

    public static final String[] TABLE_HEAD = { "商品编号", "商品名称", "价格", "库存数量" };
    public Object[][] tableBody = new Object[20][4];
    private JTable table;

    @Autowired
    private GoodsService goodsService;

    /**
     * Create the frame.
     */
    public GoodsQueryJFrame() {
        setTitle("商品查询");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1050, 650);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        DefaultTableModel tableModel = new DefaultTableModel(tableBody, TABLE_HEAD);

        JButton btnNewButton = new JButton("返回首页");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoodsJFrame frame = SpringUtil.getBean(GoodsJFrame.class);
                frame.setVisible(true);
                dispose();
            }
        });
        btnNewButton.setBounds(872, 540, 113, 27);
        contentPane.add(btnNewButton);

//		JButton btnNewButton_1 = new JButton("设备详情查询");
//		btnNewButton_1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				DeviceDetailsJFrame frame = SpringUtil.getBean(DeviceDetailsJFrame.class);
//				frame.setVisible(true);
//				dispose();
//			}
//		});
//		btnNewButton_1.setBounds(688, 540, 138, 27);
//		contentPane.add(btnNewButton_1);
//
//		JButton btnNewButton_2 = new JButton("设备数据查询");
//		btnNewButton_2.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				DeviceDataJFrame frame = SpringUtil.getBean(DeviceDataJFrame.class);
//				frame.setVisible(true);
//				dispose();
//			}
//		});
//		btnNewButton_2.setBounds(500, 540, 143, 27);
//		contentPane.add(btnNewButton_2);

        JLabel label = new JLabel("商品名称：");
        label.setBounds(40, 38, 85, 18);
        contentPane.add(label);

//		JLabel label_1 = new JLabel("减少数量：");
//		label_1.setBounds(407, 35, 85, 18);
//		contentPane.add(label_1);

        projectName = new JTextField();
        projectName.setBounds(125, 35, 234, 24);
        contentPane.add(projectName);
        projectName.setColumns(10);

//		deviceNo = new JTextField();
//		deviceNo.setText("");
//		deviceNo.setColumns(10);
//		deviceNo.setBounds(487, 32, 234, 24);
//		contentPane.add(deviceNo);

        JButton btnNewButton_3 = new JButton("查询");
        btnNewButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
//                DBHelper.getConnection();
//                DBHelper.openStatement();
//                String sql = "select * from goods where name='" + projectName.getText() + "'";
//                ResultSet rs = DBHelper.query(sql);
                  String nname=projectName.getText();



//                String nname="";
                int iid = 0,nnumber=0;
                Double pprice = 0.0;
//                try {
//                    while(rs.next()){
//                        nname =rs.getString("name");
//                        iid =rs.getInt("id");
//                        pprice =rs.getDouble("price");
//                        nnumber =rs.getInt("number");
//                    }
//
//                } catch (SQLException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
                List<GoodsDAO> message =  goodsService.queryGoodsMessage(nname);
                System.out.println("message:"+message);
//                tableBody[0][0] = iid;
//                tableBody[0][1] = nname;
//                tableBody[0][2] = pprice;
//                tableBody[0][3] = nnumber;
                tableBody[0][0] = message.get(0).getId();
                tableBody[0][1] = message.get(0).getName();
                tableBody[0][2] = message.get(0).getPrice();
                tableBody[0][3] = message.get(0).getNumber();
//                DBHelper.close();

//				List<DeviceStatus> list = constructionDeviceService.findDeviceStatus(projectName.getText(),
//						deviceNo.getText());
//				tableBody = new Object[list.size()][4];
//				for (int i = 0; i < list.size(); i++) {
//					tableBody[i][0] = list.get(i).getConstruction_id();
//					tableBody[i][1] = list.get(i).getId();
//					tableBody[i][2] = list.get(i).getProject_name();
//					tableBody[i][3] = list.get(i).getDvice_no();
//				}
                tableModel.setDataVector(tableBody, TABLE_HEAD);
                JOptionPane.showMessageDialog(null, "查询成功");
            }
        });
        btnNewButton_3.setBounds(480, 30, 113, 27);
        contentPane.add(btnNewButton_3);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(40, 87, 950, 432);
        contentPane.add(scrollPane);
        table = new JTable();
        table.setFont(new Font("微软雅黑", Font.PLAIN, 18));
        table.setRowHeight(30);
        table.setModel(tableModel);
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class,r);
        scrollPane.setColumnHeaderView(table);
        scrollPane.setViewportView(table);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }

}
