package com.sise.springbootswing;

import com.sise.springbootswing.Mapper.GoodsMapper;
import com.sise.springbootswing.Service.GoodsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;

@SpringBootTest
class SpringbootswingApplicationTests {
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    GoodsService goodsService;
    /**
     * 判断当前平台是否支持托盘
     */
    @Test
    void contextLoads() {
        Boolean result =  SystemTray.isSupported();
        System.out.println(result);
    }

    /**
     * 测试查询列表
     */
    @Test
    void test1(){
        System.out.println(goodsMapper.queryAllList());
    }

    /**
     * 测试查询商品数据
     */
    @Test
    public void test2(){
        System.out.println(goodsMapper.queryGoodsMessage("测试"));
    }

    /**
     * 测试插入数据
     */
//    @Test
//    public void test3(){
//        System.out.println(goodsMapper.addGoodmessage("测试类","101","18"));
//    }

    /**
     * 测试查询列表
     */
    @Test
    void test4(){
        System.out.println(goodsService.queryAllList());
    }

    /**
     * 测试查询商品数据
     */
    @Test
    public void test5(){
        System.out.println(goodsService.queryGoodsMessage("测试"));
    }

    /**
     * 测试插入数据
     */
//    @Test
//    public void test6(){
//        System.out.println(goodsService.addGoodmessage("测试类s","102","19"));
//    }

    /**
     * 测试更新数据
     */
//    @Test
//    public void test7(){
//        System.out.println(goodsService.updateGoodsMessage("12","测试类s","1020","190"));
//    }
    /**
     * 测试删除数据
     */
    @Test
    public void test8(){
        System.out.println(goodsService.deleteGoods("13"));
    }

}
